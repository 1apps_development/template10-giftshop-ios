import UIKit
import SwiftyJSON
import SDWebImage

// MARK: - ProductListDelegate
protocol ProductListDelegate {
  func getdata(subcategory_id:String,maincategory_id:String,categories_name:String)
}

class MenuCell: UITableViewCell {
  @IBOutlet weak var img_Categories: UIImageView!
  @IBOutlet weak var lbl_title: UILabel!
}

class MenuVC: UIViewController {

  @IBOutlet weak var tableview_menu: UITableView!
  @IBOutlet weak var btn_login: UIButton!

  var menuListArray = [JSON]()
  var delegate: ProductListDelegate!
  var Home_Categories_Array = [[String:String]]()

  // MARK: - viewDidLoad
  override func viewDidLoad() {
    super.viewDidLoad()
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      self.btn_login.isHidden = false
    }
    else {
      self.btn_login.isHidden = true
    }
    let urlString = API_URL + "navigation"
    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
    let params: NSDictionary = ["theme_id":APP_THEME]
    self.Webservice_navigation(url: urlString, params: params, header: headers)
  }

  @IBAction func btn_login(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
    nav.navigationBar.isHidden = true
    keyWindow?.rootViewController = nav
  }
  @IBAction func btnTap_twitter(_ sender: Any) {
    guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_TwitterURL)) else {
      return
    }
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }

  @IBAction func btnTap_instagram(_ sender: Any) {
    guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_InstaURL)) else {
      return
    }
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }

  @IBAction func btnTap_Cancel(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }

  @IBAction func btnTap_Messanger(_ sender: Any) {
    guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_MessageURL)) else {
      return
    }
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }

  @IBAction func btnTap_youTube(_ sender: Any) {
    guard let url = URL(string: UserDefaultManager.getStringFromUserDefaults(key: UD_YoutubeURL)) else {
      return
    }
    if UIApplication.shared.canOpenURL(url) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
  }
}

extension MenuVC: UITableViewDelegate,UITableViewDataSource {

  // MARK: - numberOfRowsInSection
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.menuListArray.count
  }
  // MARK: - heightForRowAt
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50.0
  }
  // MARK: - cellForRowAt
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableview_menu.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
    let data = self.menuListArray[indexPath.item]
    cell.lbl_title.text = data["name"].stringValue
    return cell
  }
  // MARK: - didSelectRowAt
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let data = self.menuListArray[indexPath.row]
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = data["id"].stringValue
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

extension MenuVC {
  // MARK: - navigation api calling
  func Webservice_navigation(url:String, params:NSDictionary, header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].arrayValue
        self.menuListArray = jsondata
        self.tableview_menu.reloadData()
        self.tableview_menu.delegate = self
        self.tableview_menu.dataSource = self
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}

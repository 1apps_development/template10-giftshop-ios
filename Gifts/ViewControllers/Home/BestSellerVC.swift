import UIKit
import SwiftyJSON

class BestSellerVC: UIViewController {

  // MARK: - IBOutlets
  @IBOutlet weak var collectionview_bestSeller: UICollectionView!
  @IBOutlet weak var lbl_bannerText: UILabel!
  @IBOutlet weak var lbl_count: UILabel!
  @IBOutlet weak var ScrollView: UIScrollView!
  @IBOutlet weak var lbl_totalCount : UILabel!
  @IBOutlet weak var view_Empty : UIView!
  @IBOutlet weak var img_Banner: UIImageView!
  @IBOutlet weak var lbl_itemName: UILabel!
  @IBOutlet weak var Height_colliectionview_bestSellers: NSLayoutConstraint!

  // MARK: - variables
  var pageIndex = 1
  var lastIndex = 0
  var SelectedSubCategoryid = String()
  var Home_Categories_Array = [[String:String]]()
  var Featured_Products_Array = [[String:String]]()
  var Featuredproducts_Guest_Array = [JSON]()
  var maincategory_id = String()
  var MainCategorie = String()
  var ishome = String()
  var product_id = String()
  var Selected_Variant_id = String()
  var Bestseller_Products_Array = [[String:String]]()
  var isNavigetMenu = String()
  var subcategory_id = String()

  // MARK: - viewDidLoad
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view_Empty.isHidden = false
    cornerRadius(viewName: self.lbl_count, radius: self.lbl_count.frame.height / 2)
    ScrollView.delegate = self
    let urlString = API_URL + "product_banner"
    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
    let params: NSDictionary = ["theme_id":APP_THEME]
    self.Webservice_ProductBanner(url: urlString, params: params, header: headers)
  }

  @IBAction func btnTap_Back(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }

  @IBAction func btnTap_Cart(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }


  // MARK: - viewWillAppear
  override func viewWillAppear(_ animated: Bool) {
    self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      self.pageIndex = 1
      self.lastIndex = 0
      let urlString2 = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
      let headers2:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params2: NSDictionary = ["maincategory_id":self.maincategory_id,"theme_id":APP_THEME]
      self.Webservice_Categorysproduct(url: urlString2, params: params2, header: headers2)
    }
    else {
      self.pageIndex = 1
      self.lastIndex = 0
      let urlString2 = API_URL + "categorys-product?page=\(self.pageIndex)"
      let headers2:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params2: NSDictionary = ["maincategory_id":self.maincategory_id,"theme_id":APP_THEME]
      self.Webservice_Categorysproduct(url: urlString2, params: params2, header: headers2)
    }
  }

  // MARK: - scrollViewDidScroll
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if (Int(self.ScrollView.contentOffset.y) >=  Int(self.ScrollView.contentSize.height - self.ScrollView.frame.size.height)) {
      if self.pageIndex != self.lastIndex {
        self.pageIndex = self.pageIndex + 1
        if self.Featured_Products_Array.count != 0 {
          if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
            let urlString = API_URL + "categorys-product-guest?page=\(self.pageIndex)"
            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
            let params: NSDictionary = ["maincategory_id":self.maincategory_id,
                                        "theme_id":APP_THEME]
            self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
          }
          else {
            let urlString = API_URL + "categorys-product?page=\(self.pageIndex)"
            let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
            let params: NSDictionary = ["maincategory_id":self.maincategory_id,
                                        "theme_id":APP_THEME]
            self.Webservice_Categorysproduct(url: urlString, params: params, header: headers)
          }
        }
      }
    }
  }
}

extension BestSellerVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

  // MARK: - numberOfItemsInSection
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.Featured_Products_Array.count
  }

  // MARK: - cellForItemAt
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.collectionview_bestSeller.dequeueReusableCell(withReuseIdentifier: "HomeCollectionviewCell1", for: indexPath) as! HomeCollectionviewCell1
    let data = Featured_Products_Array[indexPath.item]
    cell.lbl_itemname.text = data["name"]!
    let ItemPrice = formatter.string(for: data["final_price"]!.toDouble)
    cell.lbl_price.text = ItemPrice!
    cell.img_item.sd_setImage(with: URL(string: IMG_URL + data["cover_image_path"]!), placeholderImage: UIImage(named: ""))
    cell.lbl_tag.text = "\(data["tag_api"]!.uppercased())"
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      cell.btn_favrites.isHidden = true
    }
    else {
      cell.btn_favrites.isHidden = false
    }
    if data["in_whishlist"]! == "false" {
      cell.btn_favrites.setImage(UIImage.init(named: "ic_hart"), for: .normal)
    }
    else {
      cell.btn_favrites.setImage(UIImage.init(named: "ic_hartfill"), for: .normal)
    }
    cell.lbl_curruncy.text = UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name)
    cell.btn_favrites.tag = indexPath.row
    cell.btn_favrites.addTarget(self, action: #selector(btnTap_Like), for: .touchUpInside)
    cell.btn_cart.tag = indexPath.row
    cell.btn_cart.addTarget(self, action: #selector(btnTap_cart), for: .touchUpInside)
    return cell
  }

  // MARK: - sizeForItemAt
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: (UIScreen.main.bounds.width - 48.0)/2, height: 340.0)
  }

  // MARK: - didSelectItemAt
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let data = self.Featured_Products_Array[indexPath.item]
    let vc = self.storyboard?.instantiateViewController(identifier: "ItemDetailsVC") as! ItemDetailsVC
    vc.item_id = data["id"]!
    self.navigationController?.pushViewController(vc, animated: true)
  }

  // MARK: - cart button action
  @objc func btnTap_cart(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let data = Featured_Products_Array[sender.tag]
      if UserDefaults.standard.value(forKey: UD_GuestObj) != nil {
        var Guest_Array = UserDefaultManager.getCustomObjFromUserDefaultsGuest(key: UD_GuestObj) as! [[String:String]]
        var iscart = false
        var cartindex = Int()
        for i in 0..<Guest_Array.count {
          if Guest_Array[i]["product_id"]! == data["id"]! && Guest_Array[i]["variant_id"]! == data["default_variant_id"]! {
            iscart = true
            cartindex = i
          }
        }
        if iscart == false {
          let cartobj = ["product_id": data["id"]!,
                         "image": data["cover_image_path"]!,
                         "name": data["name"]!,
                         "orignal_price": data["orignal_price"]!,
                         "discount_price": data["discount_price"]!,
                         "final_price": data["final_price"]!,
                         "qty": "1",
                         "variant_id": data["default_variant_id"]!,
                         "variant_name": data["variant_name"]!]
          Guest_Array.append(cartobj)
          UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
          UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          let alert = UIAlertController(title: nil, message: "\(data["name"]!) add successfully", preferredStyle: .alert)
          let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: true)
          }
          let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
            self.navigationController?.pushViewController(vc, animated: true)
          }
          alert.addAction(photoLibraryAction)
          alert.addAction(cameraAction)
          self.present(alert, animated: true, completion: nil)
        }
        else {
          let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
          let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in

            var data = Guest_Array[cartindex]
            data["qty"] = "\(Int(data["qty"]!)! + 1)"
            Guest_Array.remove(at: cartindex)
            Guest_Array.insert(data, at: cartindex)
            UserDefaultManager.setCustomObjToUserDefaultsGuest(CustomeObj: Guest_Array, key: UD_GuestObj)
            UserDefaultManager.setStringToUserDefaults(value: "\(Guest_Array.count)", key: UD_CartCount)
          }
          let noAction = UIAlertAction(title: "No", style: .destructive)
          alertVC.addAction(noAction)
          alertVC.addAction(yesAction)
          self.present(alertVC,animated: true,completion: nil)
        }
      }
      self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    }
    else {
      let data = self.Featured_Products_Array[sender.tag]
      self.product_id = data["id"]!
      self.Selected_Variant_id = data["default_variant_id"]!
      let urlString = API_URL + "addtocart"
      let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
      let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                  "variant_id":data["default_variant_id"]!,
                                  "qty":"1",
                                  "product_id":data["id"]!,
                                  "theme_id":APP_THEME]
      self.Webservice_Cart(url: urlString, params: params, header: headers)
    }
  }

  // MARK: - like button action
  @objc func btnTap_Like(sender:UIButton) {
    if UserDefaultManager.getStringFromUserDefaults(key: UD_userId) == "" {
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
      let nav : UINavigationController = UINavigationController(rootViewController: objVC)
      nav.navigationBar.isHidden = true
      keyWindow?.rootViewController = nav
    }
    else {
      let data = Featured_Products_Array[sender.tag]
      if data["in_whishlist"]! == "false" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"add",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "add", sender: sender.tag, isselect: "Featured")
      }
      else if data["in_whishlist"]! == "true" {
        let urlString = API_URL + "wishlist"
        let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
        let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                    "product_id":data["id"]!,
                                    "wishlist_type":"remove",
                                    "theme_id":APP_THEME]
        self.Webservice_wishlist(url: urlString, params: params, header: headers, wishlisttype: "remove", sender: sender.tag, isselect: "Featured")
      }
    }
  }
}

extension BestSellerVC {
  // MARK: - Categorysproduct api calling
  func Webservice_Categorysproduct(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let jsondata = jsonResponse!["data"].dictionaryValue
        self.lbl_totalCount.text = "Found "+jsondata["total"]!.stringValue+" products"
       // self.lbl_itemName.text = jsondata["data"]!["category_name"].stringValue
        if self.pageIndex == 1 {
          self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
          self.Featured_Products_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,
                            "name":data["name"].stringValue,
                            "tag_api":data["tag_api"].stringValue,
                            "cover_image_path":data["cover_image_path"].stringValue,
                            "final_price":data["final_price"].stringValue,
                            "in_whishlist":data["in_whishlist"].stringValue,
                            "default_variant_id":data["default_variant_id"].stringValue,
                            "orignal_price":data["original_price"].stringValue,
                            "discount_price":data["discount_price"].stringValue,
                            "variant_name":data["default_variant_name"].stringValue]
          self.Featured_Products_Array.append(productObj)
        }
        self.collectionview_bestSeller.reloadData()
        self.collectionview_bestSeller.delegate = self
        self.collectionview_bestSeller.dataSource = self
        self.view_Empty.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
          self.Height_colliectionview_bestSellers.constant = self.collectionview_bestSeller.contentSize.height
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - Cart api calling
  func Webservice_Cart(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["data"]["count"].stringValue, key: UD_CartCount)
        self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
        let alert = UIAlertController(title: nil, message: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"), preferredStyle: .alert)
        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
          self.dismiss(animated: true)
        }
        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
          let vc = self.storyboard?.instantiateViewController(identifier: "CartVC") as! CartVC
          self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(photoLibraryAction)
        alert.addAction(cameraAction)
        self.present(alert, animated: true, completion: nil)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else if status == "0" {
        let alertVC = UIAlertController(title: Bundle.main.displayName!, message: ALREADYCART_CONFIRM_MESSAGE, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
          let urlString = API_URL + "cart-qty"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["user_id":UserDefaultManager.getStringFromUserDefaults(key: UD_userId),
                                      "product_id":self.product_id,
                                      "variant_id":self.Selected_Variant_id,
                                      "quantity_type":"increase",
                                      "theme_id":APP_THEME]
          self.Webservice_CartQty(url: urlString, params: params, header: headers)
        }
        let noAction = UIAlertAction(title: "No", style: .destructive)
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        self.present(alertVC,animated: true,completion: nil)
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - CartQty api calling
  func Webservice_CartQty(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        UserDefaultManager.setStringToUserDefaults(value: jsonResponse!["count"].stringValue, key: UD_CartCount)
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }

  //MARK: - wishlist api calling
  func Webservice_wishlist(url:String, params:NSDictionary,header:NSDictionary,wishlisttype:String,sender:Int,isselect:String) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        if isselect == "best" { }
        else if isselect == "trending" { }
        else {
          if wishlisttype == "add" {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "true"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.collectionview_bestSeller.reloadData()
          } else {
            var data = self.Featured_Products_Array[sender]
            data["in_whishlist"]! = "false"
            self.Featured_Products_Array.remove(at: sender)
            self.Featured_Products_Array.insert(data, at: sender)
            self.collectionview_bestSeller.reloadData()
          }
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
  // MARK: - ProductBanner api calling
  func Webservice_ProductBanner(url:String, params:NSDictionary,header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1" {
        let data = jsonResponse!["data"].dictionaryValue
        let themjson = data["them_json"]!["products-header-banner"].dictionaryValue
        self.lbl_bannerText.text! = themjson["products-header-banner-title-text"]!.stringValue
        self.img_Banner.sd_setImage(with: URL(string: IMG_URL + themjson["products-header-banner"]!.stringValue), placeholderImage: UIImage(named: "placeHolder"))
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}

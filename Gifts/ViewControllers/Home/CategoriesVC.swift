import UIKit
import SwiftyJSON

class CategoryCollectionviewCell: UICollectionViewCell {

  @IBOutlet weak var img_category: UIImageView!
  @IBOutlet weak var lbl_category: UILabel!
}

class CategoriesVC: UIViewController, UIScrollViewDelegate {

  // MARK: - IBOutlets
  @IBOutlet weak var height_collectionview: NSLayoutConstraint!
  @IBOutlet weak var collectionview_category: UICollectionView!
  @IBOutlet weak var lbl_count: UILabel!
  @IBOutlet weak var scroll_view: UIScrollView!
  @IBOutlet weak var View_Circle: UIView!
  @IBOutlet weak var View_1: UIView!
  @IBOutlet weak var View_2: UIView!
  @IBOutlet weak var View_3: UIView!
  @IBOutlet weak var View_4: UIView!
  @IBOutlet weak var cate_1_view_1: UILabel!
  @IBOutlet weak var btn_cate_1_view_1: UIButton!
  @IBOutlet weak var cate_1_view_2: UILabel!
  @IBOutlet weak var btn_cate_1_view_2: UIButton!
  @IBOutlet weak var cate_2_view_2: UILabel!
  @IBOutlet weak var btn_cate_2_view_2: UIButton!
  @IBOutlet weak var cate_1_view_3: UILabel!
  @IBOutlet weak var btn_cate_1_view_3: UIButton!
  @IBOutlet weak var cate_2_view_3: UILabel!
  @IBOutlet weak var btn_cate_2_view_3: UIButton!
  @IBOutlet weak var cate_3_view_3: UILabel!
  @IBOutlet weak var btn_cate_3_view_3: UIButton!
  @IBOutlet weak var cate_1_view_4: UILabel!
  @IBOutlet weak var btn_cate_1_view_4: UIButton!
  @IBOutlet weak var cate_2_view_4: UILabel!
  @IBOutlet weak var btn_cate_2_view_4: UIButton!
  @IBOutlet weak var cate_3_view_4: UILabel!
  @IBOutlet weak var btn_cate_3_view_4: UIButton!
  @IBOutlet weak var cate_4_view_4: UILabel!
  @IBOutlet weak var btn_cate_4_view_4: UIButton!

  var isSelectedAgain = Bool()
  var pageIndex = 1
  var lastIndex = 0
  var Home_Categories_Array = [[String:String]]()

  override func viewDidLoad() {
    super.viewDidLoad()
    self.View_1.isHidden = true
    self.View_2.isHidden = true
    self.View_3.isHidden = true
    self.View_4.isHidden = true
    self.scroll_view.delegate = self
    NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: Notification.Name(rawValue: "NOTIFICATION_CENTER_TAB"), object: nil)
  }

  // MARK: - viewWillAppear
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    cornerRadius(viewName: self.lbl_count, radius: self.lbl_count.frame.height / 2)
    self.lbl_count.text = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    self.tabBarController?.tabBar.isHidden = false
    self.View_Circle.isHidden = false
    self.pageIndex = 1
    self.lastIndex = 0
    let urlString = API_URL + "home-categoty?page=\(self.pageIndex)"
    let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
    let params: NSDictionary = ["theme_id":APP_THEME]
    self.Webservice_category(url: urlString, params: params, header: headers)
  }

  // MARK: - notificationReceived
  @objc func notificationReceived(_ noti: Notification){
    isSelectedAgain = true
    if isSelectedAgain {
      UIView.animate(withDuration: 0.3) {
        self.View_Circle.isHidden = false
      }
    }
  }

  @IBAction func btnTap_cart(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }

  // MARK: - scrollViewDidScroll
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if (Int(self.scroll_view.contentOffset.y) >=  Int(self.scroll_view.contentSize.height - self.scroll_view.frame.size.height)) {
      self.View_Circle.isHidden = true
      if self.pageIndex != self.lastIndex {
        self.pageIndex = self.pageIndex + 1
        if self.Home_Categories_Array.count != 0 {
          let urlString = API_URL + "home-categoty?page=\(self.pageIndex)"
          let headers:NSDictionary = ["Content-type": "application/json","Authorization":"\(UserDefaultManager.getStringFromUserDefaults(key: UD_TokenType)) \(UserDefaultManager.getStringFromUserDefaults(key: UD_BearerToken))"]
          let params: NSDictionary = ["theme_id":APP_THEME]
          self.Webservice_category(url: urlString, params: params, header: headers)
        }
      }
    }
  }

  var lastContentOffset: CGFloat = 0

  // MARK: - scrollViewWillBeginDragging
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    self.lastContentOffset = scrollView.contentOffset.y
  }
}

// MARK: - Button Actions
extension CategoriesVC {
  @IBAction func btnTap_Close(_ sender: UIButton) {
    self.View_Circle.isHidden = true
  }
  // View_1
  @IBAction func btnTap_cate_1_view_1(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[0]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  // View_2
  @IBAction func btnTap_cate_1_view_2(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[0]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_2_view_2(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[1]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  // View_3
  @IBAction func btnTap_cate_1_view_3(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[0]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_2_view_3(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[1]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_3_view_3(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[2]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  // View_4
  @IBAction func btnTap_cate_1_view_4(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[0]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_2_view_4(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[1]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_3_view_4(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[2]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
  @IBAction func btnTap_cate_4_view_4(_ sender: UIButton) {
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = self.Home_Categories_Array[3]["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
}


extension CategoriesVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

  // MARK: - numberOfItemsInSection
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.Home_Categories_Array.count
  }

  // MARK: - cellForItemAt
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.collectionview_category.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionviewCell", for: indexPath) as! CategoryCollectionviewCell
    let data = self.Home_Categories_Array[indexPath.item]
    cell.lbl_category.text = data["name"]!
    cell.img_category.sd_setImage(with: URL(string: IMG_URL + data["image_path"]!), placeholderImage: UIImage(named: ""))
    return cell
  }

  // MARK: - sizeForItemAt
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: (UIScreen.main.bounds.width - 48.0)/2, height: ((UIScreen.main.bounds.width - 48.0)/2) * 1.7)
  }

  // MARK: - didSelectItemAt
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let data = self.Home_Categories_Array[indexPath.item]
    let vc = self.storyboard?.instantiateViewController(identifier: "BestSellerVC") as! BestSellerVC
    vc.maincategory_id = data["id"]!
    vc.ishome = "yes"
    self.navigationController?.pushViewController(vc, animated: true)
  }
}

extension CategoriesVC {

  // MARK: - category api calling
  func Webservice_category(url:String, params:NSDictionary, header:NSDictionary) -> Void {
    WebServices().CallGlobalAPI(url: url, headers: header, parameters:params, httpMethod: "POST", progressView:true, uiView:self.view, networkAlert: true) {(_ jsonResponse:JSON? , _ statusCode:String) in
      let status = jsonResponse!["status"].stringValue
      if status == "1"  {
        let jsondata = jsonResponse!["data"].dictionaryValue
        if self.pageIndex == 1 {
          self.lastIndex = Int(jsondata["last_page"]!.stringValue)!
          self.Home_Categories_Array.removeAll()
        }
        let Featuredprodcutdata = jsondata["data"]!.arrayValue
        for data in Featuredprodcutdata {
          let productObj = ["id":data["id"].stringValue,"name":data["name"].stringValue,"image_path":data["image_path"].stringValue,"status":data["status"].stringValue,"category_id":data["category_id"].stringValue,"category_item":data["category_item"].stringValue,"icon_path":data["icon_path"].stringValue]
          self.Home_Categories_Array.append(productObj)
        }
        let first4 = Array(self.Home_Categories_Array.prefix(4))
        if first4.count == 1 {
          self.View_1.isHidden = false
          self.cate_1_view_1.text = self.Home_Categories_Array[0]["name"]!
          let cateimage_1 = UIImageView()
          cateimage_1.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[0]["icon_path"]!), placeholderImage: UIImage(named: ""))
          self.btn_cate_1_view_1.setImage(cateimage_1.image, for: .normal)
          DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.btn_cate_1_view_1.setImage(cateimage_1.image, for: .normal)
          }
          cornerRadius(viewName: self.btn_cate_1_view_1, radius: self.btn_cate_1_view_1.frame.height / 2)
        }
        if first4.count == 2 {
          self.View_2.isHidden = false
          self.cate_1_view_2.text = self.Home_Categories_Array[0]["name"]!
          self.cate_2_view_2.text = self.Home_Categories_Array[1]["name"]!
          let cateimage_1 = UIImageView()
          let cateimage_2 = UIImageView()
          cateimage_1.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[0]["icon_path"]!), placeholderImage: UIImage(named: ""))
          cateimage_2.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[1]["icon_path"]!), placeholderImage: UIImage(named: ""))
          self.btn_cate_1_view_2.setImage(cateimage_1.image, for: .normal)
          self.btn_cate_2_view_2.setImage(cateimage_2.image, for: .normal)
          DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.btn_cate_1_view_2.setImage(cateimage_1.image, for: .normal)
            self.btn_cate_2_view_2.setImage(cateimage_2.image, for: .normal)
          }
          cornerRadius(viewName: self.btn_cate_1_view_2, radius: self.btn_cate_1_view_2.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_2_view_2, radius: self.btn_cate_2_view_2.frame.height / 2)
        }
        if first4.count == 3  {
          self.View_3.isHidden = false
          self.cate_1_view_3.text = self.Home_Categories_Array[0]["name"]!
          self.cate_2_view_3.text = self.Home_Categories_Array[1]["name"]!
          self.cate_3_view_3.text = self.Home_Categories_Array[2]["name"]!
          let cateimage_1 = UIImageView()
          let cateimage_2 = UIImageView()
          let cateimage_3 = UIImageView()
          cateimage_1.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[0]["icon_path"]!), placeholderImage: UIImage(named: ""))
          cateimage_2.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[1]["icon_path"]!), placeholderImage: UIImage(named: ""))
          cateimage_3.sd_setImage(with: URL(string: IMG_URL + self.Home_Categories_Array[2]["icon_path"]!), placeholderImage: UIImage(named: ""))
          self.btn_cate_1_view_3.setImage(cateimage_1.image, for: .normal)
          self.btn_cate_2_view_3.setImage(cateimage_2.image, for: .normal)
          self.btn_cate_3_view_3.setImage(cateimage_3.image, for: .normal)
          DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.btn_cate_1_view_3.setImage(cateimage_1.image, for: .normal)
            self.btn_cate_2_view_3.setImage(cateimage_2.image, for: .normal)
            self.btn_cate_3_view_3.setImage(cateimage_3.image, for: .normal)
          }
          cornerRadius(viewName: self.btn_cate_1_view_3, radius: self.btn_cate_1_view_3.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_2_view_3, radius: self.btn_cate_2_view_3.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_3_view_3, radius: self.btn_cate_3_view_3.frame.height / 2)
        }
        if first4.count == 4 {
          self.View_4.isHidden = false
          cornerRadius(viewName: self.btn_cate_1_view_4, radius: self.btn_cate_1_view_4.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_2_view_4, radius: self.btn_cate_2_view_4.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_3_view_4, radius: self.btn_cate_3_view_4.frame.height / 2)
          cornerRadius(viewName: self.btn_cate_4_view_4, radius: self.btn_cate_4_view_4.frame.height / 2)
          self.cate_1_view_4.text = self.Home_Categories_Array[0]["name"]!
          self.cate_2_view_4.text = self.Home_Categories_Array[1]["name"]!
          self.cate_3_view_4.text = self.Home_Categories_Array[2]["name"]!
          self.cate_4_view_4.text = self.Home_Categories_Array[3]["name"]!
          let cateimage_1 = UIImageView()
          let cateimage_2 = UIImageView()
          let cateimage_3 = UIImageView()
          let cateimage_4 = UIImageView()
           
            let original1 = IMG_URL + self.Home_Categories_Array[0]["icon_path"]!
                if let encoded = original1.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                  let url = URL(string: encoded)
                {
                    cateimage_1.sd_setImage(with: url, placeholderImage: UIImage(named: ""))

//                    cateimage_1.image = cateimage_1.image?.withRenderingMode(.alwaysTemplate)
//                    cateimage_1.tintColor = UIColor(named: "BlueColor")
                }
            let original2 = IMG_URL + self.Home_Categories_Array[1]["icon_path"]!
                if let encoded = original2.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                  let url = URL(string: encoded)
                {
                    cateimage_2.sd_setImage(with: url, placeholderImage: UIImage(named: ""))

//                    cateimage_2.image = cateimage_2.image?.withRenderingMode(.alwaysTemplate)
//                    cateimage_2.tintColor = UIColor(named: "BlueColor")
                }
            let original3 = IMG_URL + self.Home_Categories_Array[2]["icon_path"]!
                if let encoded = original3.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                  let url = URL(string: encoded)
                {
                    cateimage_3.sd_setImage(with: url, placeholderImage: UIImage(named: ""))

//                    cateimage_3.image = cateimage_3.image?.withRenderingMode(.alwaysTemplate)
//                    cateimage_3.tintColor = UIColor(named: "BlueColor")
                }
            let original4 = IMG_URL + self.Home_Categories_Array[3]["icon_path"]!
                if let encoded = original4.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                  let url = URL(string: encoded)
                {
                    cateimage_4.sd_setImage(with: url, placeholderImage: UIImage(named: ""))

//                    cateimage_4.image = cateimage_4.image?.withRenderingMode(.alwaysTemplate)
//                    cateimage_4.tintColor = UIColor(named: "BlueColor")
                }


//          self.btn_cate_1_view_4.setImage(cateimage_1.image, for: .normal)
//          self.btn_cate_2_view_4.setImage(cateimage_2.image, for: .normal)
//          self.btn_cate_3_view_4.setImage(cateimage_3.image, for: .normal)
//          self.btn_cate_4_view_4.setImage(cateimage_4.image, for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.btn_cate_1_view_4.setImage(cateimage_1.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                self.btn_cate_1_view_4.tintColor = UIColor(named: "BlueColor")
            self.btn_cate_2_view_4.setImage(cateimage_2.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                self.btn_cate_2_view_4.tintColor = UIColor(named: "BlueColor")

            self.btn_cate_3_view_4.setImage(cateimage_3.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                self.btn_cate_3_view_4.tintColor = UIColor(named: "BlueColor")

            self.btn_cate_4_view_4.setImage(cateimage_4.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                self.btn_cate_4_view_4.tintColor = UIColor(named: "BlueColor")

          }
        }
        self.collectionview_category.delegate = self
        self.collectionview_category.dataSource = self
        self.collectionview_category.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          self.height_collectionview.constant = self.collectionview_category.contentSize.height
        }
      }
      else if status == "9" {
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
        UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
      }
      else {
        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: jsonResponse!["data"]["message"].stringValue.replacingOccurrences(of: "\\n", with: "\n"))
      }
    }
  }
}


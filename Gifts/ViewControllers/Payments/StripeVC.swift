import UIKit
import Stripe
import SwiftyJSON

class StripeVC: UIViewController {

  @IBOutlet weak var btn_Paynow: UIButton!
  var cardField = STPPaymentCardTextField()
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupCardTextField()
  }

  // MARK: - setupCardFiled
  func setupCardTextField() {
    print("Setup card textfield and set frame according to UI")
    cardField.frame = CGRect(x: 20, y: 200, width: self.view.frame.size.width - 40, height: 60)
    cardField.delegate = self

    self.view.addSubview(cardField)
    self.btn_Paynow.isHidden = true

    print("Create/Get customer from Stripe using backend server api call (I have used PHP)")
    StripeAPIClient.shared.getCustomer(UserDefaultManager.getStringFromUserDefaults(key: UD_emailId), name: UserDefaultManager.getStringFromUserDefaults(key: UD_userFirstName), success: { (response) in
      print(response)
      print("We have recieved Customer detals, and we will pass it in charge api when needed.")
    }) { (error) in
      print(error ?? "some error")
    }
  }

  @IBAction func btnTap_Back(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
  }

  // MARK: - BtnTap_PayNow
  @IBAction func BtnTap_PayNow(_ sender: UIButton) {
    cardField.resignFirstResponder()
    print("Get token using card details")

    let card = cardField.cardParams
    let cardParams: STPCardParams = STPCardParams()
    cardParams.number = cardField.cardNumber
    cardParams.expMonth = UInt(cardField.expirationMonth)
    cardParams.expYear = UInt(cardField.expirationYear)
    cardParams.cvc = cardField.cvc
    STPAPIClient.shared.createToken(withCard: cardParams) { (token, error) in
      print("STPAPIClient token: \(token?.tokenId ?? "No token received")")
      if let error = error {
        print(error)
      }
      else if let token = token {
        print("Card token received :\(JSON(token.allResponseFields))")
        print("In backend, Call charge api of stripe")
        StripeAPIClient.shared.completeChargeForCustomTextField(token.tokenId, customerID: UserDefaultManager.getStringFromUserDefaults(key: UD_userId), amount: 10, currency: UserDefaultManager.getStringFromUserDefaults(key: UD_currency_Name), description: "Strip Testing", success:
                                                                  { (json) in
          let jsonTemp = JSON(json as Any)
          if let detail = jsonTemp.dictionaryObject {
            print("Card has been charged :\(detail)")
          }
        }, failure: { (errorJson) in
          print(errorJson)
          print("error in completeChargeForCustomTextField")
          print(errorJson ?? "its a error")
        })
      }
    }
  }
}

// MARK: STPPaymentCardTextFieldDelegate
extension StripeVC : STPPaymentCardTextFieldDelegate {
  func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
    self.btn_Paynow.isHidden = !textField.isValid
  }
}

import UIKit

class WelcomeVC: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }

  @IBAction func btnTap_Login(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginSelectionVC") as! LoginSelectionVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }

  @IBAction func btnTap_Register(_ sender: UIButton) {
    let objVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterSelectionVC") as! RegisterSelectionVC
    self.navigationController?.pushViewController(objVC, animated: true)
  }

  @IBAction func btnTap_Guest(_ sender: UIButton) {
    UserDefaultManager.setStringToUserDefaults(value: "", key: UD_userId)
    UserDefaultManager.setStringToUserDefaults(value: "", key: UD_BearerToken)
    UserDefaultManager.setStringToUserDefaults(value: "", key: UD_TokenType)

    let count = UserDefaultManager.getStringFromUserDefaults(key: UD_CartCount)
    if count == "" || count == "N/A" {
      UserDefaultManager.setStringToUserDefaults(value: "", key: UD_CartCount)
    }
    let objVC = MainstoryBoard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
    let TabViewController = MainstoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
    let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
    appNavigation.setNavigationBarHidden(true, animated: true)
    keyWindow?.rootViewController = TabViewController
  }
}

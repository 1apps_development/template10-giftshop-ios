import UIKit

class ChangePasswordSuccessVC: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  @IBAction func btnTap_GoToStore(_ sender: UIButton) {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let objVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
    nav.navigationBar.isHidden = true
    keyWindow?.rootViewController = nav
  }
}
